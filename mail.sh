# Copyright 2016 Andreea Mocanu & Alexandru Mocanu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------
# Authors:
#   andreea.oana.mocanu@gmail.com
#   alexandru.mocanu@gmail.com
#
# Sources:
#   https://bitbucket.org/am-bits/friendlymail

[[ -z "${_mail_sh_included}" ]] && _mail_sh_included='yes' || return

source "${FRIENDLY_BASH}"
source "${FRIENDLY_MAIL_ROOT}/mail.properties"

# Usage: mail::send_external <receivers> <email_subject> <email_body>
#
# Sends a simple text email using the configured external SMTP server.
# All configurations of the external SMTP server can be found in the mail.properties file.
#
#   receivers       list of addresses to send the email to, separated by comma
#   email_subject   the email subject (on a single line)
#   email_body      the body of the email (on multiple lines if needed)
function mail::send_external() {
    util::mandatory_program_is_installed 'swaks'
    util::mandatory_min_arg_count 3 "$@"
    local readonly _to="$1"
    local readonly _subject="$2"
    local readonly _body="${*:3}"
 
    swaks --to "${_to}"\
          --from "${MAIL_EXTERNAL_SMTP_DEFAULT_FROM}"\
          --h-Subject "${_subject}"\
          --body "${_body}"\
          --server "${MAIL_EXTERNAL_SMTP_SERVER}"\
          --auth LOGIN\
          --auth-user "${MAIL_EXTERNAL_SMTP_USERNAME}"\
          --auth-password "${MAIL_EXTERNAL_SMTP_PASSWORD}"\
          --port "${MAIL_EXTERNAL_SMTP_PORT}"\
    > /dev/null
}


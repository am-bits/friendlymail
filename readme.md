# FriendlyMail #

FriendlyMail is a [FriendlyBash](https://bitbucket.org/am-bits/friendlybash) extension, useful for sending emails.

## How to install ##

1. Go to the *Dependencies* section below and check that everything listed there is installed.

1. Download the [latest version](https://bitbucket.org/am-bits/friendlymail/downloads/friendlymail_latest.tar) of FriendlyMail. Older versions can be found [here](https://bitbucket.org/am-bits/friendlymail/downloads).

1. Extract the contents of the tar archive at a chosen location on your machine, e.g.

        tar -xvf friendlymail_latest.tar -C /my/path/to/friendlymail

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_MAIL_ROOT=/my/path/to/friendlymail

## How to use ##

1. Import the FriendlyMail library in your script:

		#!/bin/bash
		source "${FRIENDLY_MAIL_ROOT}/mail.sh"

1. Edit the mail.properties file and change the properties according to your needs.

1. You can now call any FriendlyMail function, e.g.

		mail::send_external

Run this `grep` search to see all available FriendlyMail functions:

	grep -nor "function [a-z][a-z_]*::[a-z_]\+()" ${FRIENDLY_MAIL_ROOT}

The documentation for each function can be found together with the function definition in the `${FRIENDLY_MAIL_ROOT}/mail.sh` file.

## Dependencies ##

* swaks v20130209.0
* FriendlyBash v1.0

## Development ##

### Setup ###

1. Clone the repository.

1. Add the following definitions to your `/etc/environment` file:

        FRIENDLY_MAIL_ROOT=/my/friendlymail/repository

1. Install [ShellCheck](https://github.com/koalaman/shellcheck).

### Development guidelines ###

Same as the [FriendlyBash](https://bitbucket.org/am-bits/friendlybash) development guidelines.
